import sqlite3
from yaml import safe_load

class SQLite_DB:

	def __init__(self):

		self.db = sqlite3.connect("db.sqlite3",check_same_thread=False)
		self.db.row_factory = lambda C, R: { c[0]: R[i] for i, c in enumerate(C.description) }
		with open("scripts/sql_statements.yml") as f:
			self.queries = safe_load(f)

	def exec_sql(self,query_id,*args):
		cursor = self.db.cursor()
		try:
			cursor.execute(self.queries[query_id],args)
			return cursor.fetchall()
		finally:
			cursor.close()
