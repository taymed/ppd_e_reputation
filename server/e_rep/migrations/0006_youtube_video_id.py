# Generated by Django 3.0.6 on 2020-06-02 16:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('e_rep', '0005_remove_twitter_reply_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='youtube',
            name='video_id',
            field=models.CharField(default='unknown', max_length=256),
            preserve_default=False,
        ),
    ]
