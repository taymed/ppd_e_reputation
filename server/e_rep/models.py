from django.db import models

# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=256,null=False,blank=False,unique=True)

    def __str__(self):
        return self.name

class Features(models.Model):

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    twitter_text_count_pos = models.PositiveIntegerField(default=0,null=True,blank=True)
    twitter_like_sum_pos = models.PositiveIntegerField(default=0,null=True,blank=True)
    twitter_retweet_sum_pos = models.PositiveIntegerField(default=0,null=True,blank=True)
    twitter_text_count_neg = models.PositiveIntegerField(default=0,null=True,blank=True)
    twitter_like_sum_neg = models.PositiveIntegerField(default=0,null=True,blank=True)
    twitter_retweet_sum_neg = models.PositiveIntegerField(default=0,null=True,blank=True)
    youtube_text_count_pos = models.PositiveIntegerField(default=0,null=True,blank=True)
    youtube_text_count_neg = models.PositiveIntegerField(default=0,null=True,blank=True)
    youtube_like_count = models.PositiveIntegerField(default=0,null=True,blank=True)
    youtube_view_count = models.PositiveIntegerField(default=0,null=True,blank=True)
    amazon_text_count_pos = models.PositiveIntegerField(default=0,null=True,blank=True)
    amazon_rating_avg_pos = models.FloatField(default=0,null=True,blank=True)
    amazon_text_count_neg = models.PositiveIntegerField(default=0,null=True,blank=True)
    amazon_rating_avg_neg = models.FloatField(default=0,null=True,blank=True)
    date = models.DateField(editable=False)

    class Meta:
        verbose_name = "Feature"
        verbose_name_plural = "Features"


class GoodText(models.Model):

    product = models.OneToOneField(Product, primary_key=True, on_delete=models.CASCADE)
    twitter_most_pos_text = models.TextField(max_length=2000,default="",null=True,blank=True)
    twitter_most_pos_text_likes = models.PositiveIntegerField(default=0,null=True,blank=True)
    twitter_most_neg_text = models.TextField(max_length=2000,default="",null=True,blank=True)
    twitter_most_neg_text_likes = models.PositiveIntegerField(default=0,null=True,blank=True)
    amazon_most_pos_text = models.TextField(max_length=2000,default="",null=True,blank=True)
    amazon_most_pos_text_rating = models.PositiveIntegerField(default=0,null=True,blank=True)
    amazon_most_neg_text = models.TextField(max_length=2000,default="",null=True,blank=True)
    amazon_most_neg_text_rating = models.PositiveIntegerField(default=0,null=True,blank=True)
    pos_word_cloud = models.TextField(max_length=5000,default="",null=True,blank=True)
    neg_word_cloud = models.TextField(max_length=5000,default="",null=True,blank=True)

class Twitter(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    text = models.TextField(max_length=2000,default="",null=True,blank=True)
    sentiment = models.IntegerField(choices=[(1,'positive'),(-1,'negative')])
    favorite_count = models.PositiveIntegerField(default=0,null=True,blank=True)
    retweet_count = models.PositiveIntegerField(default=0,null=True,blank=True)
    date = models.DateField()

class Youtube(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    video_id = models.CharField(max_length=256,null=False,blank=False)
    text = models.TextField(max_length=2000,default="",null=True,blank=True)
    sentiment = models.IntegerField(choices=[(1,'positive'),(-1,'negative')])
    like_count = models.PositiveIntegerField(default=0,null=True,blank=True)
    view_count = models.PositiveIntegerField(default=0,null=True,blank=True)
    date = models.DateField()

class Amazon(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    text = models.TextField(max_length=2000,default="",null=True,blank=True)
    sentiment = models.IntegerField(choices=[(1,'positive'),(-1,'negative')])
    rating = models.PositiveIntegerField(default=0,null=True,blank=True)
    variant = models.CharField(max_length=256,null=True,blank=True)
    date = models.DateField()

class Scraping_Url(models.Model):
    url = models.URLField(primary_key=True)
    site = models.CharField(max_length=256,null=False)
    product_name = models.CharField(max_length=256,null=False)