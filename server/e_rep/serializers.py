from rest_framework import serializers

from .models import Product, Twitter, Youtube, Amazon, Scraping_Url, Features, GoodText

class Product_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

class Twitter_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Twitter
        fields = '__all__'

class Youtube_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Youtube
        fields = '__all__'

class Amazon_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Amazon
        fields = '__all__'

class Scraping_Urls_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Scraping_Url
        fields = '__all__'

class Features_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Features
        fields = '__all__'

class GoodText_Serializer(serializers.ModelSerializer):
    class Meta:
        model = GoodText
        fields = '__all__'